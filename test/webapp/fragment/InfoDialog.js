sap.ui.define([
    "sap/ui/base/ManagedObject"
], function (ManagedObject) {
    "use strict";

    return ManagedObject.extend("de.otto.template.fragment.InfoDialog", {

        constructor: function (oController) {
            this._oController = oController;
            this._oView = oController.getView();
        },

        exit: function () {
            delete this._oView;
        },

        open: function () {
            let oDialog = this._oDialog;
            const oView = this._oView;

            // create dialog lazily
            if (!oDialog) {

                const oFragmentController = {
                    onPressClose: function (oEvent) {
                        oDialog.close();
                    }
                };

                oDialog = sap.ui.xmlfragment(oView.getId(), "de.otto.template.fragment.InfoDialog", oFragmentController);
                jQuery.sap.syncStyleClass(oView.getController().getOwnerComponent().getContentDensityClass(), oView, oDialog);

                oView.addDependent(oDialog);
                this._oDialog = oDialog;
            }

            oDialog.open();
            return oDialog;
        }
    });
});
