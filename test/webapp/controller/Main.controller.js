sap.ui.define([
    "./BaseController",
    "../fragment/InfoDialog",
    "sap/m/MessageToast"
], function (BaseController, InfoDialog, MessageToast) {
    "use strict";

    return BaseController.extend("de.otto.template.controller.Main", {

        /**
         * Lifecycle hooks
         */

        onInit: function () {
            this._oInfoDialog = new InfoDialog(this);
        },

        /**
         * Event handler
         */

        onPressDeleteButton: function (oEvent) {
            let sMessage = "Hello";
            const sWorld = "World";

            sMessage = [sMessage, sWorld].join(" ");
            MessageToast.show(sMessage);
        },

        onPressHintButton: function (oEvent) {
            this._oInfoDialog.open();
        }

        /**
         * Private methods
         */
    });
});
