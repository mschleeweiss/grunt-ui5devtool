/*
 * grunt-ui5devtool
 * https://gitlab.com/mschleeweiss/grunt-ui5devtool
 *
 * Copyright (c) 2018 Marc Schleeweiß
 * Licensed under the MIT license.
 */

"use strict";

module.exports = function (grunt) {
    var path = require("path");
    grunt.ui5config = require("./lib/config")(grunt, process.cwd());

    require("load-grunt-config")(grunt, {
        // path to task.js files, defaults to grunt dir
        configPath: path.join(process.cwd(), "grunt"),

        // auto grunt.initConfig
        init: true,

        // data passed into config.  Can use with <%= test %>
        data: {
            test: false
        },

        // use different function to merge config files
        mergeFunction: require("recursive-merge"),

		// can optionally pass options to load-grunt-tasks.
		// If you set to false, it will disable auto loading tasks.
		loadGruntTasks: {
            pattern: "grunt-*",
            scope: "devDependencies"
        },

        //can post process config object before it gets passed to grunt
        postProcess: function (config) {
        },

        //allows to manipulate the config object before it gets merged with the data object
        preMerge: function (config, data) {
        }
    });

};
