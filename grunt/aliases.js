module.exports = function (grunt, config) {
    "use strict";

    return {

        // Server task
        "serve": function (target) {
            target = target || "src";

            const aTasks = [
                "configureProxies",
                "string-replace:i18n_utf8",
                "openui5_connect:" + target,
                "watch"
            ];

            grunt.task.run(aTasks);
        },

        // Lint task
        "lint": function (env) {
            env = env || "dev"

            const aTasks = [
                "eslint:" + env
            ];

            grunt.task.run(aTasks);
        },

        // Build task
        "build": function () {
            const aTasks = [
                "test:prod",
                "clean",
                "openui5_preload",
                "copy"
            ];

            grunt.task.run(aTasks);
        },

        // Execute after build
        "deploy": function () {
            const aTasks = [
                "nwabap_ui5uploader"
            ];

            grunt.task.run(aTasks);
        },

        // Default task (called when just running "grunt")
        "default": [
            "serve"
        ]

    };
};
