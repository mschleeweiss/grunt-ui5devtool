module.exports = function (grunt, config) {
    "use strict";

    return {
        webapp: "test/webapp",
        dist: "dist",
        bowerComponents: "bower_components"
    };
};
