module.exports = function (grunt, config) {
    "use strict";

    return {
        component: {
            options: {
                resources: {
                    cwd: "<%= dir.webapp %>",
                    prefix: "de.otto.template".replace(/\./g, "/")
                },
                dest: "<%= dir.dist %>",
                compatVersion: "1.44"
            },
            components: true
        }
    };
};
