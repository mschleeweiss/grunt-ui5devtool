module.exports = function (grunt, config) {
    "use strict";

    return {
        options: {
            conn: {
                server: grunt.ui5config.sap.nwbc.host,
                useStrictSSL: false
            },
            auth: {
                user: grunt.ui5config.sap.auth.user,
                pwd: grunt.ui5config.sap.auth.pwd
            }
        },
        "upload_build": {
            options: {
                ui5: {
                    language: "DE",
                    package: grunt.ui5config.sap.nwbc.package,
                    bspcontainer: grunt.ui5config.sap.nwbc.bspContainer,
                    bspcontainer_text: grunt.ui5config.sap.nwbc.bspContainerText,
                    transportno: grunt.ui5config.sap.nwbc.transport,
                    calc_appindex: true
                },
                resources: {
                    cwd: "<%= dir.dist %>",
                    src: "**/*.*"
                }
            }
        }
    };
};
