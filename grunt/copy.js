module.exports = function (grunt, config) {
    return {
        dist: {
            files: [{
                expand: true,
                cwd: "<%= dir.webapp %>",
                src: [
                    "**",
                    "!localService/**",
                    "!test/**"
                ],
                dest: "<%= dir.dist %>"
            }]
        }
    }
};