module.exports = function (grunt, config) {
    "use strict";

    return {
        dev: {
            options: {
                fix: true,
                rules: {
                    "no-console": 0,
                    "no-debugger": 0
                }
            },
            src: ["<%= dir.webapp %>"]
        },
        prod: {
            options: {
                fix: false
            },
            src: ["<%= dir.webapp %>"]
        }
    };
};
