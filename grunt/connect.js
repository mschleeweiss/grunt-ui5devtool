/*global require*/
module.exports = function (grunt, config) {
    "use strict";

    return {
        options: {
            base: "public",
            hostname: "localhost",
            open: true,
            livereload: true,
            middleware: function (connect, options, defaultMiddleware) {
                const oProxy = require("grunt-connect-proxy/lib/utils").proxyRequest;
                return [
                    oProxy
                ].concat(defaultMiddleware);
            }
        },
        proxies: [
            {
                context: "/sap",
                host: grunt.ui5config.sap.nwbc.host,
                port: grunt.ui5config.sap.nwbc.port,
                https: false
            }
        ],
        src: {},
        dist: {}
    }
};