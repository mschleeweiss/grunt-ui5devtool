module.exports = function (grunt, config) {
    "use strict";

    return {
        all: {
            files: "<%= dir.webapp %>/**/*",
            options: {
                livereload: true
            }
        },
        js: {
            files: "<%= dir.webapp %>/**/*.js",
            tasks: ["test:dev"]
        }
    };
};
