module.exports = function (grunt, config) {
    "use strict";

    return {
        "i18n_utf8": {
            src: "./node_modules/connect-openui5/lib/properties.js",
            dest: "./node_modules/connect-openui5/lib/properties.js",
            options: {
                replacements: [{
                    pattern: /ISO-8859-1/g,
                    replacement: function () {
                        return "UTF-8";
                    }
                }]
            }
        },
        "d73_ui5lib": {
            src: "./dist/index.html",
            dest: "./dist/index.html",
            options: {
                replacements: [{
                    pattern: /src="resources\/sap-ui-core.js"/ig,
                    replacement: function () {
                        return "src=\"/sap/bc/bsp/otto/ui5/resources/sap-ui-core.js\"";
                    }
                }]
            }
        }
    };
};
