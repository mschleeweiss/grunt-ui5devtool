
module.exports = function (grunt, config) {
    "use strict";

    return {
        options: {
            resources: [
                "<%= dir.bowerComponents %>/openui5-sap.m/resources",
                "<%= dir.bowerComponents %>/openui5-sap.ui.core/resources",
                "<%= dir.bowerComponents %>/openui5-sap.ui.layout/resources",
                "<%= dir.bowerComponents %>/openui5-sap.ui.unified/resources",
                "<%= dir.bowerComponents %>/openui5-sap.ui.table/resources",
                "<%= dir.bowerComponents %>/openui5-sap.uxap/resources",
                "<%= dir.bowerComponents %>/openui5-themelib_sap_belize/resources"
            ]
        },
        src: {
            options: {
                appresources: "<%= dir.webapp %>"
            }
        },
        dist: {
            options: {
                appresources: "<%= dir.dist %>"
            }
        }
    };
};
