module.exports = function (grunt, projectPath) {
    "use strict";

    const defaults = {
        sap: {
            nwbc: {
                protocol: "http",
                host: "myhost",
                port: 8020,
                bspContainer: "ZZ_WORKLIST_TEM",
                bspContainerText: "UI5 App",
                package: "$TMP",
                transport: null
            },
            scp: {
                secure: false
            }
        },
        build: {
            resourcePath: {
                src: undefined //https://sapui5.hana.ondemand.com/
            }
        },
        dir: {
            webapp: "<%= dir.webapp %>",
            dist: "<%= dir.dist %>"
        }
    };

    const loadConfig = function (projectPath) {
        const configDirPath = projectPath + "/configs";
        const buildConfigPath = configDirPath + "/ui5devtool.json";

        const loadedConfig = {};
        try {
            loadedConfig = require(buildConfigPath);
        } catch (error) {
            console.log(error);
        }
        const config = {...defaults, ...loadedConfig};

        config.sap.auth = {
            user: grunt.option("nwbc-user"),
            pwd: grunt.option("nwbc-pw")
        }
        return config;
    }

    return loadConfig(projectPath);
};
